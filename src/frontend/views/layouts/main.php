<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 image-header-block center-block">
            <img src="/img/head.png" class="image-header ">
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
<nav>
   <?php $menuItems = [
    ['label' => 'Главная', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Регистрация', 'url' => ['/user/default/signup']];
    $menuItems[] = ['label' => 'Войти', 'url' => ['/user/default/login']];
    } else {
        //$menuItems[] = ['label' => Yii::t(' ', 'Профиль'), 'url' => ['/user/profile/view', 'nickname' => Yii::$app->user->identity->getNickname()]];
        $menuItems[] = ['label' => 'Профиль', 'url' => ['/user/profile/view', 'nickname' => Yii::$app->user->identity->getNickname()]];
        $menuItems[] = ['label' => 'Запостить', 'url' => ['/post/default/create']];
        $menuItems[] = '<li>'
        . Html::beginForm(['/user/default/logout'], 'post')
        . Html::submitButton(
        'Выйти из под (' . Yii::$app->user->identity->username . ') <i class="nav-item"> </i>',
        ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
    }
    echo Nav::widget([
    'options' => ['class' => 'nav nav-pills center-block navbar-right'],
    'items' => $menuItems,
    ]); ?>

</nav>
<div class="container-fluid">
    <?= Alert::widget() ?>
    <?= $content ?>
    </div>

    <footer>
    </footer>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


    <?php
//    NavBar::begin([
//        'brandLabel' => Yii::$app->name,
//        'brandUrl' => Yii::$app->homeUrl,
//        'options' => [
//            'class' => 'navbar-inverse navbar-fixed-top',
//        ],
//    ]);

    ?>






