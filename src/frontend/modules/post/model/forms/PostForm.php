<?php

namespace frontend\modules\post\model\forms;

use Yii;
use yii\base\Model;
use frontend\models\Post;
use common\models\User;
use Intervention\Image\ImageManager;
use frontend\models\event\PostCreatedEvent;

class PostForm extends Model
{


    /** @var integer исходная ширина */
    private $sourceWidth;

    /** @var integer исходная высота */
    private $sourceHeight;

    /** @var string Путь к картинке */
    private $path;

    /** @var string mime тип картинки */
    private $mimeType;

    /** Ключи результирующего массива метода getimagesize*/
    const WIDTH_SIZE_IMAGE = 0;
    const HEIGHT_SIZE_IMAGE = 1;
    const MIME_TYPE_IMAGE = 'mime';


    const MAX_DESCRIPTION_LENGHT = 1000;
    const EVENT_POST_CREATED = 'post_created';


    public $picture;
    public $description;

    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['picture'], 'file',
                'skipOnEmpty' => false,
                'extensions' => ['jpg', 'png'],
                'checkExtensionByMimeType' => true,
                'maxSize' => $this->getMaxFileSize()],
            [['description'], 'string', 'max' => self::MAX_DESCRIPTION_LENGHT],
        ];
    }

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->on(self::EVENT_AFTER_VALIDATE, [$this, 'resize']);
        $this->on(self::EVENT_POST_CREATED, [Yii::$app->feedService, 'addToFeeds']);


    }

    public function resize()
    {
        $keepProportion = false;
        $width = Yii::$app->params['postPicture']['maxWidth'];
        $height = Yii::$app->params['postPicture']['maxHeight'];
        $finalWidth = $width;
        $finalHeight = $height;
        if ($keepProportion) {
            $sourceRelation = $this->sourceWidth / $this->sourceHeight;
            if ($width / $height > $sourceRelation) {
                $finalWidth = $height * $sourceRelation;
            } else {
                $finalHeight = $width / $sourceRelation;
            }
        }

        if ($this->mimeType == 'image/png') {
            $this->resizeImagePng($finalWidth, $finalHeight);
        }

        if (in_array($this->mimeType, ['image/jpg', 'image/jpeg'])) {
            $this->resizeImageJpg($finalWidth, $finalHeight);
        }
    }

    /**
     *  Изменение размера картинки в формате png*
     *
     * @param int $width Необходимая ширина файла
     * @param int $height Необходимая высота файла
     */
    private function resizeImagePng(int $width, int $height)
    {
        $src = imagecreatefrompng($this->path);
        $dst = imagecreatetruecolor($width, $height);

        imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $this->sourceWidth, $this->sourceHeight);
        imagepng($dst, $this->path, 5);
    }

    /**
     * Изменение размера картинки jpg
     *
     * @param int $width Необходимая ширина файла
     * @param int $height Необходимая высота файла
     */
    private function resizeImageJpg(int $width, int $height)
    {
        $src = imagecreatefromjpeg($this->path);
        $dst = imagecreatetruecolor($width, $height);

        imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $this->sourceWidth, $this->sourceHeight);
        imagejpeg($dst, $this->path);
    }

    /**
     * @return boolean
     */
    public function save()
    {
        if ($this->validate()) {

            $post = new Post();
            $post->description = $this->description;
            $post->created_at = time();
            $post->filename = Yii::$app->storage->saveUploadedFile($this->picture);
            $post->user_id = $this->user->getId();

            if ($post->save(false)) {
                $event = new PostCreatedEvent();
                $event->user = $this->user;
                $event->post = $post;

                $this->trigger(self::EVENT_POST_CREATED, $event);
                return true;
            }
        }
        return false;

    }

    /**
     * Maximum size of the uploaded file
     * @return integer
     */
    private function getMaxFileSize()
    {
        return Yii::$app->params['maxFileSize'];
    }

}

